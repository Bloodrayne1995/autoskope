﻿Namespace Enums

    Public Enum CarStatus As Integer
        DRIVING = 0
        HALTING = 3
        PARKING = 4
    End Enum

End Namespace