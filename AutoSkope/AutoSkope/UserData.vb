﻿Imports System.IO
Imports System.Net
Imports System.Security
Imports System.Xml

Public Class UserData

#Region "Internal"

    Private _username As SecureString
    Private _password As SecureString
    Private _appversion As String = "2.0"

#End Region

#Region "Properties"

    Public Property username As String
        Get
            Return GetStringFromSecureString(_username)
        End Get
        Set(value As String)
            If _username IsNot Nothing Then
                _username.Dispose()
            End If
            _username = New SecureString
            For Each c As Char In value.ToCharArray
                _username.AppendChar(c)
            Next
            _username.MakeReadOnly()
        End Set
    End Property

    Public Property password As String
        Get
            Return GetStringFromSecureString(_password)
        End Get
        Set(value As String)
            If _password IsNot Nothing Then
                _password.Dispose()
            End If
            _password = New SecureString
            For Each c As Char In value.ToCharArray
                _password.AppendChar(c)
            Next
            _password.MakeReadOnly()
        End Set
    End Property

    Public ReadOnly Property appversion As Double
        Get
            Return _appversion
        End Get
    End Property

#End Region

#Region "Constructors"

    Public Sub New(ByVal appversion As Double)
        _appversion = appversion
    End Sub

    Public Sub New(ByVal fileName As FileInfo)
        Dim doc As New XmlDocument
        doc.Load(fileName.FullName)

        UserName = doc.SelectSingleNode("/userdata/username").InnerText
        Password = doc.SelectSingleNode("/userdata/password").InnerText
        _appversion = doc.SelectSingleNode("/userdata/appversion").InnerText


    End Sub

#End Region

#Region "Methods"

    Private Function GetStringFromSecureString(ByRef sec As SecureString) As String
        Return New NetworkCredential("", sec).Password
    End Function

    Public Function ToJSON() As String
        Dim str As String = ""
        str &= "{" & vbCrLf
        str &= Chr(34) & "username" & Chr(34) & ": " & Chr(34) & UserName & Chr(34) & vbCrLf
        str &= Chr(34) & "password" & Chr(34) & ": " & Chr(34) & Password & Chr(34) & vbCrLf
        str &= Chr(34) & "appversion" & Chr(34) & ": " & AppVersion & vbCrLf

        str &= "}"

        Return str
    End Function

#End Region

End Class
