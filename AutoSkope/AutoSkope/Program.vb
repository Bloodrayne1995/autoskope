Imports System
Imports System.IO
Imports AutoSkope.Elements

Module Program
    Sub Main(args As String())
        Console.WriteLine("Hello World!")

        AppContext.SetSwitch("System.Net.Http.UseSocketsHttpHandler", False)

        If args.Length = 0 Then
            Console.WriteLine("Please provide userdata file as argument")
            Exit Sub
        End If

        Dim f As New FileInfo(args(0))

        Dim uData As New UserData(f)

        Dim client As New WebClient()
        Console.WriteLine(client.Login(uData))


        Dim list As List(Of Car) = client.GetCars()

        If list IsNot Nothing Then
            For Each c As Car In list
                Console.WriteLine(c.Name)
            Next
        End If

        Console.ReadKey()

    End Sub
End Module
