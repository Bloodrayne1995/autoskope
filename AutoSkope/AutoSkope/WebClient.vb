﻿Imports System.Net
Imports System.Net.Http
Imports System.Text.Json
Imports AutoSkope.Elements

Public Class WebClient

#Region "Internal"

    Private _client As HttpClient
    Private _handler As New WinHttpHandler
    Private _cookieContainer As New CookieContainer()
    Private _sessionID As String = ""

#End Region

#Region "Properties"

    Public ReadOnly Property Client As HttpClient
        Get
            Return _client
        End Get
    End Property

#End Region

#Region "Constructors"

    Public Sub New()
        _handler.CookieContainer = _cookieContainer
        _handler.CookieUsePolicy = CookieUsePolicy.UseSpecifiedCookieContainer

#If DEBUG Then
        'Add Telerik Fiddler for debugging purpose
        Dim prx As New WebProxy
        prx.Address = New Uri("http://localhost:8888")
        _handler.WindowsProxyUsePolicy = WindowsProxyUsePolicy.UseCustomProxy
        _handler.Proxy = prx
#End If

        _client = New HttpClient(_handler)
    End Sub

#End Region

#Region "Methods"


    Public Function Login(ByRef uData As UserData) As Boolean
        Dim cnt As New ByteArrayContent(Text.Encoding.ASCII.GetBytes(uData.ToJSON))


        Dim resp As HttpResponseMessage = _client.PostAsync("https://portal.autoskope.de/scripts/ajax/login.php", cnt).Result

        For Each c As Cookie In _handler.CookieContainer.GetCookies(New Uri("https://portal.autoskope.de"))
            Console.WriteLine(c.Name & " ==> " & c.Value & "  |  " & c.Domain)
            If c.Name Like "PHP*" Then
                _sessionID = c.Value
            End If
        Next

        Return resp.StatusCode = Net.HttpStatusCode.OK
    End Function




    Public Function GetCars() As List(Of Car)
        Dim resp As HttpResponseMessage = _client.PostAsync("https://portal.autoskope.de/scripts/ajax/app/info.php", Nothing).Result

        If Not resp.StatusCode = Net.HttpStatusCode.OK Then
            Return Nothing
        End If

        Dim l As List(Of Car) = JsonSerializer.Deserialize(Of List(Of Car))(resp.Content.ReadAsByteArrayAsync().Result)

        Return l

    End Function




#End Region


End Class
