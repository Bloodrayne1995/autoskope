﻿Namespace Elements
    Public Class Car

#Region "Internal"

        Private _id As Integer = -1
        Private _carttype_id As Integer = -1
        Private _markerURI As String = ""
        Private _markerIndex As String = ""
        Private _name As String = ""
        Private _marker As String = ""
        Private _gpsdisplay As Boolean = False
        Private _voltdisplay As Boolean = False
        Private _batdisplay As Boolean = False
        Private _gsmdisplay As Boolean = False
        Private _dashcardisplay As Boolean = False
        Private _speedmeterchart As Boolean = False
        Private _speedchart As Boolean = False
        Private _usagechart As Boolean = False
        Private _voltagechart As Boolean = False
        Private _vibrationalarmdisplay As Boolean = False
        Private _skopeoutputdisplay As Boolean = False
        Private _hdop As Double = 0.5
        Private _ex_pow As Double = 0.5
        Private _bat_pow As Double = 3.3
        Private _viAlarm As Boolean = False



#End Region

#Region "Properties"

        Public Property Id As Integer
            Get
                Return _id
            End Get
            Set(value As Integer)
                _id = value
            End Set
        End Property

        Public Property Carttype_id As Integer
            Get
                Return _carttype_id
            End Get
            Set(value As Integer)
                _carttype_id = value
            End Set
        End Property

        Public Property MarkerURI As String
            Get
                Return _markerURI
            End Get
            Set(value As String)
                _markerURI = value
            End Set
        End Property

        Public Property MarkerIndex As String
            Get
                Return _markerIndex
            End Get
            Set(value As String)
                _markerIndex = value
            End Set
        End Property

        Public Property Name As String
            Get
                Return _name
            End Get
            Set(value As String)
                _name = value
            End Set
        End Property

        Public Property Marker As String
            Get
                Return _marker
            End Get
            Set(value As String)
                _marker = value
            End Set
        End Property

        Public Property Gpsdisplay As Boolean
            Get
                Return _gpsdisplay
            End Get
            Set(value As Boolean)
                _gpsdisplay = value
            End Set
        End Property

        Public Property Voltdisplay As Boolean
            Get
                Return _voltdisplay
            End Get
            Set(value As Boolean)
                _voltdisplay = value
            End Set
        End Property

        Public Property Batdisplay As Boolean
            Get
                Return _batdisplay
            End Get
            Set(value As Boolean)
                _batdisplay = value
            End Set
        End Property

        Public Property Gsmdisplay As Boolean
            Get
                Return _gsmdisplay
            End Get
            Set(value As Boolean)
                _gsmdisplay = value
            End Set
        End Property

        Public Property Dashcardisplay As Boolean
            Get
                Return _dashcardisplay
            End Get
            Set(value As Boolean)
                _dashcardisplay = value
            End Set
        End Property

        Public Property Speedmeterchart As Boolean
            Get
                Return _speedmeterchart
            End Get
            Set(value As Boolean)
                _speedmeterchart = value
            End Set
        End Property

        Public Property Speedchart As Boolean
            Get
                Return _speedchart
            End Get
            Set(value As Boolean)
                _speedchart = value
            End Set
        End Property

        Public Property Usagechart As Boolean
            Get
                Return _usagechart
            End Get
            Set(value As Boolean)
                _usagechart = value
            End Set
        End Property

        Public Property Voltagechart As Boolean
            Get
                Return _voltagechart
            End Get
            Set(value As Boolean)
                _voltagechart = value
            End Set
        End Property

        Public Property Vibrationalarmdisplay As Boolean
            Get
                Return _vibrationalarmdisplay
            End Get
            Set(value As Boolean)
                _vibrationalarmdisplay = value
            End Set
        End Property

        Public Property Skopeoutputdisplay As Boolean
            Get
                Return _skopeoutputdisplay
            End Get
            Set(value As Boolean)
                _skopeoutputdisplay = value
            End Set
        End Property

        Public Property Hdop As Double
            Get
                Return _hdop
            End Get
            Set(value As Double)
                _hdop = value
            End Set
        End Property

        Public Property Ex_pow As Double
            Get
                Return _ex_pow
            End Get
            Set(value As Double)
                _ex_pow = value
            End Set
        End Property

        Public Property Bat_pow As Double
            Get
                Return _bat_pow
            End Get
            Set(value As Double)
                _bat_pow = value
            End Set
        End Property

        Public Property ViAlarm As Boolean
            Get
                Return _viAlarm
            End Get
            Set(value As Boolean)
                _viAlarm = value
            End Set
        End Property

#End Region

    End Class
End Namespace