﻿Imports AutoSkope.Enums
Namespace Elements


    Public Class Position

#Region "Internal"

        Private _st As String = ""
        Private _ctp As Integer = -1
        Private _cstt As CarStatus = CarStatus.PARKING
        Private _clrndx As Integer = 0
        Private _s As Integer = 0
        Private _dt As DateTime
        Private _park As Boolean = False
        Private _dts As DateTime
        Private _dte As DateTime
        Private _dtt As TimeSpan
        Private _pos As Integer = 0



#End Region

#Region "Properties (Generated)"

        Public Property St As String
            Get
                Return _st
            End Get
            Set(value As String)
                _st = value
            End Set
        End Property

        Public Property Ctp As Integer
            Get
                Return _ctp
            End Get
            Set(value As Integer)
                _ctp = value
            End Set
        End Property

        Public Property Cstt As CarStatus
            Get
                Return _cstt
            End Get
            Set(value As CarStatus)
                _cstt = value
            End Set
        End Property

        Public Property S As Integer
            Get
                Return _s
            End Get
            Set(value As Integer)
                _s = value
            End Set
        End Property

        Public Property Dt As Date
            Get
                Return _dt
            End Get
            Set(value As Date)
                _dt = value
            End Set
        End Property

        Public Property Park As Boolean
            Get
                Return _park
            End Get
            Set(value As Boolean)
                _park = value
            End Set
        End Property

        Public Property Dts As Date
            Get
                Return _dts
            End Get
            Set(value As Date)
                _dts = value
            End Set
        End Property

        Public Property Dte As Date
            Get
                Return _dte
            End Get
            Set(value As Date)
                _dte = value
            End Set
        End Property

        Public Property Dtt As TimeSpan
            Get
                Return _dtt
            End Get
            Set(value As TimeSpan)
                _dtt = value
            End Set
        End Property

        Public Property Pos As Integer
            Get
                Return _pos
            End Get
            Set(value As Integer)
                _pos = value
            End Set
        End Property

        Public Property Clrndx As Integer
            Get
                Return _clrndx
            End Get
            Set(value As Integer)
                _clrndx = value
            End Set
        End Property

#End Region

#Region "Properties"

        Public Property Status As String
            Get
                Return St
            End Get
            Set(value As String)
                St = value
            End Set
        End Property

        Public Property CarType As Integer
            Get
                Return _ctp
            End Get
            Set(value As Integer)
                _ctp = value
            End Set
        End Property


        Public Property CarStatus As CarStatus
            Get
                Return Cstt
            End Get
            Set(value As CarStatus)
                Cstt = value
            End Set
        End Property


        Public Property ColorIndex As Integer
            Get
                Return Clrndx
            End Get
            Set(value As Integer)
                Clrndx = value
            End Set
        End Property


        Public Property Speed As Integer
            Get
                Return S
            End Get
            Set(value As Integer)
                S = value
            End Set
        End Property


        Public Property RecordTime As DateTime
            Get
                Return Dt
            End Get
            Set(value As DateTime)
                Dt = value
            End Set
        End Property

        Public Property Parking As Boolean
            Get
                Return park
            End Get
            Set(value As Boolean)
                Park = value
            End Set
        End Property

        Public Property ParkStart As DateTime
            Get
                Return Dts
            End Get
            Set(value As DateTime)
                Dts = value
            End Set
        End Property

        Public Property ParkEnde As DateTime
            Get
                Return Dte
            End Get
            Set(value As DateTime)
                Dte = value
            End Set
        End Property

        Public Property ParkZeit As TimeSpan
            Get
                Return Dtt
            End Get
            Set(value As TimeSpan)
                Dtt = value
            End Set
        End Property

        Public Property Position As Integer
            Get
                Return Pos
            End Get
            Set(value As Integer)
                Pos = value
            End Set
        End Property


#End Region

    End Class

End Namespace